# @atlaskit/docs

## 2.5.2
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 2.5.1
- [patch] Update kind2string dependency to 0.3.1 [2c432fd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/2c432fd)

## 2.5.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 2.4.3
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 2.4.2
- [patch] Refactor code helper function to fix React re-render bug. [8dcb772](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8dcb772)

## 2.4.1

## 2.4.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 2.3.0
- [minor] Added support for JSX Elements in default prop declarations [8030309](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8030309)

## 2.2.0
- [minor] Props  component now understands how to parse members of the Array type [3eebe75](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3eebe75)

## 2.1.1
- [patch] Convert function parameters [f6c5a21](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f6c5a21)
- [patch] Convert function parameters [f6c5a21](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f6c5a21)

## 2.1.0


- [minor] corrected types and added heading option to props [bdf39b3](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bdf39b3)
- [minor] corrected types and added heading option to props [bdf39b3](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bdf39b3)

## 2.0.0
- [major] Now renders default props, consumes breaking change from extract-react-types [df9fa94](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/df9fa94)
- [major] Now renders default props, consumes breaking change from extract-react-types [df9fa94](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/df9fa94)

## 1.0.1
- [patch] Releasing 1.x as this is now stable [0b87d5c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0b87d5c)
- [patch] Releasing 1.x as this is now stable [0b87d5c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0b87d5c)

## 0.0.7
- [patch] Bump version of @atlaskit/docs everywhere [9a0ea18](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9a0ea18)
- [patch] Bump version of @atlaskit/docs everywhere [9a0ea18](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9a0ea18)
- [patch] Update react-markings dependency [71d0703](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/71d0703)
- [patch] Update react-markings dependency [71d0703](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/71d0703)

## 0.0.6
- [patch] bump icon dependency [da14956](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/da14956)
- [patch] bump icon dependency [da14956](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/da14956)

## 0.0.5
- [patch] bump consumer versions for release [c730a1c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c730a1c)
- [patch] bump consumer versions for release [c730a1c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c730a1c)
- [patch] Add documentation to editor core; introduce code formatting method to docs [a1c7e56](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a1c7e56)
- [patch] Add documentation to editor core; introduce code formatting method to docs [a1c7e56](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a1c7e56)

## 0.0.4
- [patch] Use correct dependencies  [7b178b1](7b178b1)
- [patch] Use correct dependencies  [7b178b1](7b178b1)
- [patch] Adding responsive behavior to the editor. [e0d9867](e0d9867)
- [patch] Adding responsive behavior to the editor. [e0d9867](e0d9867)
