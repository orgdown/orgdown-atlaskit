# @atlaskit/media-viewer

## 10.0.4
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 10.0.3

## 10.0.2
- [patch] Expose analyticsBackend in Media Viewer configuration [3984d91](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3984d91)

## 10.0.1

- [patch] Make MediaListViewer resilient to errors and provide proper view for processing items MSW-25 MSW-348 [1d102d1](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1d102d1)

## 10.0.0

## 9.1.1
- [patch] Fixes being unable to close Media Viewer when we open a file that is processing [5f63f6c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5f63f6c)

## 9.1.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 9.0.6
- [patch] Use media-test-helpers instead of hardcoded values [f2b92f8](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f2b92f8)

## 9.0.5
- [patch] Add max-age parameter to media queries in Media Viewer - MSW-328 [4ad5d09](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4ad5d09)

## 9.0.4

## 9.0.3
- [patch]  pass pageSize property down from MediaViewer to MediaCollectionViewer [6fd7dae](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6fd7dae)

## 9.0.2
- [patch] media-core version has changed [9865149](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9865149)

## 9.0.1
- [patch] Migrate MediaViewer to new AK repo [a0bc467](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a0bc467)

## 9.0.0 (2017-11-23)


* breaking; New component introduced: MediaViewer. ([f080bf1](https://bitbucket.org/atlassian/atlaskit/commits/f080bf1))
* breaking; MSW-289 - unify API and exposing a single MediaViewer component (issues closed: msw-289) ([f080bf1](https://bitbucket.org/atlassian/atlaskit/commits/f080bf1))
## 8.1.0 (2017-11-17)

* feature; expand media-viewer peer dependencies range on media-core ([075b97f](https://bitbucket.org/atlassian/atlaskit/commits/075b97f))
* feature; upgrade version of mediapicker to 11.1.6 and media-core to 11.0.0 across packages ([aaa7aa0](https://bitbucket.org/atlassian/atlaskit/commits/aaa7aa0))
## 8.0.0 (2017-09-18)


* breaking; media-core peer dependency has changed to strictly v 10 ([ba73022](https://bitbucket.org/atlassian/atlaskit/commits/ba73022))
* breaking; update media-core and media-test-helpers version ([ba73022](https://bitbucket.org/atlassian/atlaskit/commits/ba73022))
## 7.0.0 (2017-09-18)

* breaking; media-core peer dependency has changed to strictly v 10 ([ba73022](https://bitbucket.org/atlassian/atlaskit/commits/ba73022))
* breaking; update media-core and media-test-helpers version ([ba73022](https://bitbucket.org/atlassian/atlaskit/commits/ba73022))
## 6.1.1 (2017-09-05)

* bug fix; correctly publish type declaration files ([85a5ad2](https://bitbucket.org/atlassian/atlaskit/commits/85a5ad2))


## 6.1.0 (2017-08-11)

* feature; bump :allthethings: ([f4b1375](https://bitbucket.org/atlassian/atlaskit/commits/f4b1375))




## 6.0.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 6.0.0 (2017-05-26)


* fix; fix typo in API: 'occurrenceKey' ([bf68d9a](https://bitbucket.org/atlassian/atlaskit/commits/bf68d9a))


* breaking; 'occurenceKey' renamed to 'occurrenceKey'

## 5.1.0 (2017-05-25)


* feature; add custom configuration to media-viewer ([4a1ad37](https://bitbucket.org/atlassian/atlaskit/commits/4a1ad37))

## 5.0.0 (2017-05-22)


* fix; fix tests ([9d80311](https://bitbucket.org/atlassian/atlaskit/commits/9d80311))


* feature; use media-core 8.0.0 ([0387a76](https://bitbucket.org/atlassian/atlaskit/commits/0387a76))


* breaking; Bump media-core to 8.0.0

## 4.3.1 (2017-04-27)


* fix; update legal copy to be more clear. Not all modules include ADG license. ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 4.3.0 (2017-04-26)


* fix; update legal copy and fix broken links for component README on npm. New contribution and ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))
* fix; updated media packages key words and maintainers ([01bcbc5](https://bitbucket.org/atlassian/atlaskit/commits/01bcbc5))


* feature; use /image endpoint for images ([c4fdea5](https://bitbucket.org/atlassian/atlaskit/commits/c4fdea5))

## 4.2.0 (2017-04-13)


* feature; add media file list viewer ([c6185c8](https://bitbucket.org/atlassian/atlaskit/commits/c6185c8))

## 4.1.0 (2017-04-12)


* feature; add lazy loading to media collection viewer ([9394310](https://bitbucket.org/atlassian/atlaskit/commits/9394310))

## 4.0.0 (2017-04-11)


* feature; move media-core to peerDependency ([00de0dc](https://bitbucket.org/atlassian/atlaskit/commits/00de0dc))


* breaking; Move media-core to peerDependency in media-viewer

## 3.0.0 (2017-04-10)


* fix; refreshing token in query string no longer  modifies other params ([a2d5030](https://bitbucket.org/atlassian/atlaskit/commits/a2d5030))


null refactor media viewer adapters to inject media viewer constructor instead of us ([7b578a8](https://bitbucket.org/atlassian/atlaskit/commits/7b578a8))


* feature; integrate media collection viewer with artifact mapper ([27e2580](https://bitbucket.org/atlassian/atlaskit/commits/27e2580))


* breaking; MediaViewer adapter API changes: now requires MediaViewerConstructor.

## 2.2.0 (2017-04-07)

## 2.1.0 (2017-04-06)


* fix; fix media file attributes download url ([6012fc3](https://bitbucket.org/atlassian/atlaskit/commits/6012fc3))


* feature; add id to media file model ([b606427](https://bitbucket.org/atlassian/atlaskit/commits/b606427))
* feature; add media viewer artifact format media item mapping ([adad23b](https://bitbucket.org/atlassian/atlaskit/commits/adad23b))
* feature; add media viewer file artifact mapping ([104abe1](https://bitbucket.org/atlassian/atlaskit/commits/104abe1))
* feature; use file attribute mapper inside MediaFileViewer ([5a0e3cd](https://bitbucket.org/atlassian/atlaskit/commits/5a0e3cd))

## 1.0.0 (2017-04-04)


* feature; add media-viewer adapters ([5aee637](https://bitbucket.org/atlassian/atlaskit/commits/5aee637))
