# @atlaskit/editor-json-transformer

## 2.5.8

## 2.5.7

## 2.5.6
- [patch] JSON encoding results in invalid ADF for table nodes [8a8d663](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8a8d663)

## 2.5.5
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 2.5.4

## 2.5.3
- [patch] bump editor-common to 6.1.2 [bb7802e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bb7802e)

## 2.5.2

## 2.5.1

## 2.5.0
- [minor] Implement JSONTransformer::parse to enable ADF -> PM conversion [c8c3c9e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c8c3c9e)

## 2.4.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 2.3.1

## 2.3.0

## 2.2.0

## 2.1.11

## 2.1.10

- [patch] Change to use editor-core instead of editor-bitbucket for examples [aa0c0ac](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/aa0c0ac)

## 2.1.9

## 2.1.8




- [patch] Move prosemirror-model to be a dev-dependency [206ce2f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/206ce2f)
- [patch] Move prosemirror-model to be a dev-dependency [206ce2f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/206ce2f)

## 2.1.7

## 2.1.6

## 2.1.5

## 2.1.4

## 2.1.3

## 2.1.2

## 2.1.1

## 2.1.0

## 2.0.5

## 2.0.4

## 2.0.3
- [patch] Fix dependencies [9f9de42](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9f9de42)
- [patch] Fix dependencies [9f9de42](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9f9de42)

## 2.0.2
- [patch] Fix of the build scripts for editor-*-transformer packages [59b4ea5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/59b4ea5)
- [patch] Fix of the build scripts for editor-*-transformer packages [59b4ea5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/59b4ea5)

## 2.0.1
- [patch] Fixed linting [5ebf75d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5ebf75d)
- [patch] Fixed linting [5ebf75d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5ebf75d)

## 2.0.0
- [major] Adding separate transformer packages. [f734c01](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f734c01)
- [major] Adding separate transformer packages. [f734c01](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f734c01)
