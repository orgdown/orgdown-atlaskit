# @atlaskit/conversation

## 6.3.6
- [patch] Fixes rendering of the editor in IE11 [df91076](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/df91076)

## 6.3.5

## 6.3.4

## 6.3.3
- [patch] Fix for styled-components types to support v1.4.x [75a2375](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/75a2375)

## 6.3.2

## 6.3.1

## 6.3.0
- [minor] Adding support for reactions [1b74cff](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1b74cff)

## 6.2.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 6.1.1
- [patch] Make textFormatting and hyperlink plugins default [689aa8d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/689aa8d)

## 6.1.0
- [minor] Adding renderEditor prop [e2485f7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e2485f7)

## 6.0.0
- [major] Replacing internal store with Redux [703bb99](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/703bb99)

## 5.2.5
- [patch] Adding documentations and fixed prop-types [4430cbb](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4430cbb)

## 5.2.4

## 5.2.3
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 5.2.2
- [patch] Enabling hyperlinks in the conversation editor [a151fd3](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a151fd3)

## 5.2.1

## 5.2.0
- [minor] Add subscribe/unsubscribe methods to resource provider [5c1e2bf](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5c1e2bf)

## 5.1.3

## 5.1.2

## 5.1.1
- [patch] bump editor-common to 6.1.2 [bb7802e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bb7802e)

## 5.1.0

- [minor] Add error state and cancel/retry [0c1bb6e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0c1bb6e)

## 5.0.6

## 5.0.5

## 5.0.4

## 5.0.3
- [patch] bump mention to 9.1.1 to fix mention autocomplete bug [c7708c6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c7708c6)

## 5.0.2
- [patch] Avoid unnecessary re-render of comments [402cc7c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/402cc7c)

## 5.0.1

## 5.0.0
- [major] Added some tests for reducer/store and renamed actions [ba629ef](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ba629ef)

## 4.9.1

## 4.9.0
- [minor] Adding optimistic updates [45922f3](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/45922f3)

## 4.8.2

## 4.8.1
- [patch] Fixes the avatar next to the editor and hides reply functionality if user isn't set [0123c1d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0123c1d)

## 4.8.0



- [minor] Add optional onUserClick handler [40f2e90](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/40f2e90)

## 4.7.1

## 4.7.0
- [minor] Add emoji/mention support [846baed](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/846baed)

## 4.6.0
- [minor] Fix delete comment to accept 204 [83b5f70](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/83b5f70)

## 4.5.0
- [minor] Add comment delete functionality [e26446a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e26446a)

## 4.4.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 4.3.0
- [minor] feature/ED-3471 Add comment edit functionality [c474f67](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c474f67)

## 4.2.1

## 4.2.0

## 4.1.0

## 4.0.0
- [major] New API [41633b9](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/41633b9)

## 3.0.11

## 3.0.10
- [patch] Fix dependencies in CI [670e930](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/670e930)

## 3.0.9


- [patch] dummy changeset to initiate release [ba17f5b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ba17f5b)

## 3.0.8

## 3.0.7

## 3.0.6

## 3.0.5

## 3.0.4

## 3.0.3

## 3.0.2

## 3.0.1

## 3.0.0
- [major] Changing the API to match the service [b308326](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b308326)

## 2.0.7

## 2.0.6

## 2.0.5

## 2.0.4

## 2.0.3

## 2.0.2

## 2.0.1

## 2.0.0

- [major] Conversation Component [bc1a3a4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bc1a3a4)
