# @atlaskit/media-editor

## 4.2.1

## 4.2.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 4.1.1
- [patch] Update MediaEditor bundle to remove crypto module usage [3a779d8](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3a779d8)

## 4.1.0
- [minor] Improve mediaEditor core bundle binaries; reducing bundle size, es2015 usage and remave it to mediaEditor [91c1ce1](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/91c1ce1)

## 4.0.3
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 4.0.2

## 4.0.1
- [patch] Fix eidtorCore binary [add6f5f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/add6f5f)

## 4.0.0

## 3.4.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 3.3.8

## 3.3.7

## 3.3.6
- [patch] Copy binaries into dist folder when building media-editor [a82d5da](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a82d5da)

## 3.3.5

## 3.3.4

## 3.3.3

## 3.3.2

- [patch] bump icon dependency [da14956](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/da14956)
- [patch] bump icon dependency [da14956](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/da14956)

## 3.3.1
