# @atlaskit/task-decision

## 4.10.3
- [patch] Update links in documentation [c4f7497](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c4f7497)

## 4.10.2

## 4.10.1
- [patch] Fix for styled-components types to support v1.4.x [75a2375](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/75a2375)

## 4.10.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 4.9.1
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 4.9.0
- [patch] Fix package [4bf9e49](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4bf9e49)
- [minor] Migrated package to new repo [537be77](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/537be77)

## 4.8.1 (2018-02-06)

* bug fix; copied updated json to dist for use in mk-2 ([17f61cf](https://bitbucket.org/atlassian/atlaskit/commits/17f61cf))
## 4.8.0 (2018-01-30)

* feature; resourcedItemList uses contentAsFabricDocument rather than rawContent as content to (issues closed: fs-1677) ([a0d08af](https://bitbucket.org/atlassian/atlaskit/commits/a0d08af))
## 4.7.2 (2018-01-29)

* bug fix; object/containerAri is optional to toggle isDone for ResourcedTaskItem (issues closed: fs-1674) ([1f77a21](https://bitbucket.org/atlassian/atlaskit/commits/1f77a21))
## 4.7.1 (2018-01-18)

* bug fix; move getCurrentUser to TaskDecisionResource ([222ef0e](https://bitbucket.org/atlassian/atlaskit/commits/222ef0e))
## 4.7.0 (2018-01-16)



* feature; product can pass username to resource to prevent delayed completed by message (issues closed: fs-1539) ([b85c8ac](https://bitbucket.org/atlassian/atlaskit/commits/b85c8ac))


## 4.6.1 (2017-12-19)

* bug fix; bump packages to fixed version of analytics ([615e41c](https://bitbucket.org/atlassian/atlaskit/commits/615e41c))
* bug fix; explicit bump of analytics in task-decision ([dd94543](https://bitbucket.org/atlassian/atlaskit/commits/dd94543))
## 4.6.0 (2017-12-11)

* bug fix; use onChange prop in ResourcedTaskItem ([e56bc38](https://bitbucket.org/atlassian/atlaskit/commits/e56bc38))
* bug fix; bumped renderer and editor-common versions ([cb4fff5](https://bitbucket.org/atlassian/atlaskit/commits/cb4fff5))
* feature; fS-1461 objectAri and containerAri are optional props within ResourcedTaskItem ([4ec360c](https://bitbucket.org/atlassian/atlaskit/commits/4ec360c))
## 4.5.0 (2017-12-05)

* bug fix; restore public analytics handling in TaskItem ([6a0153d](https://bitbucket.org/atlassian/atlaskit/commits/6a0153d))
* feature; fS-1461 duplicate props between TaskItem and ResourcedTaskItem ([d27cd65](https://bitbucket.org/atlassian/atlaskit/commits/d27cd65))
## 4.4.1 (2017-11-28)

* bug fix; fix export of sample-elements support data ([0a5fc11](https://bitbucket.org/atlassian/atlaskit/commits/0a5fc11))
## 4.4.0 (2017-11-28)

* feature; update react peer dep to support ^16.0.0 ([5b041bb](https://bitbucket.org/atlassian/atlaskit/commits/5b041bb))
* feature; moved from util-shared-styles to using theme package (issues closed: fs-1527) ([213377d](https://bitbucket.org/atlassian/atlaskit/commits/213377d))
* feature; update dev dependencies to latest (issues closed: fs-1527) ([dcf7ef7](https://bitbucket.org/atlassian/atlaskit/commits/dcf7ef7))
* feature; updated internal dependencies for repo migration (issues closed: fs-1527) ([0a8fde7](https://bitbucket.org/atlassian/atlaskit/commits/0a8fde7))

## 4.3.0 (2017-11-16)

* feature; fS-1387 Bump avatar version and set Participants boundaries to scrollParent ([c7e44e9](https://bitbucket.org/atlassian/atlaskit/commits/c7e44e9))
## 4.2.0 (2017-10-05)



* feature; action/decision related analytics (issues closed: fs-1290) ([38ade4e](https://bitbucket.org/atlassian/atlaskit/commits/38ade4e))

## 4.1.0 (2017-10-05)


* feature; action/decision related analytics (issues closed: fs-1290) ([38ade4e](https://bitbucket.org/atlassian/atlaskit/commits/38ade4e))

## 4.0.5 (2017-09-21)

* bug fix; Revert code splitting of mentions/task-decisions as it introduces a performance problem (issues closed: fs-1396 / hnw-3183) ([bbecb14](https://bitbucket.org/atlassian/atlaskit/commits/bbecb14))
## 4.0.4 (2017-09-21)

* bug fix; fix :derp: ([088588f](https://bitbucket.org/atlassian/atlaskit/commits/088588f))
* bug fix; fix typescript validation error in test. ([a6f3d53](https://bitbucket.org/atlassian/atlaskit/commits/a6f3d53))
## 4.0.3 (2017-09-20)

* bug fix; workaround chromium 56 bug for background svg's blurring (issues closed: fs-1392) ([727ed6c](https://bitbucket.org/atlassian/atlaskit/commits/727ed6c))
## 4.0.2 (2017-09-19)

* bug fix; fixed jest tests when require.ensure is undefined ([245707a](https://bitbucket.org/atlassian/atlaskit/commits/245707a))
* bug fix; fixed tests for task-decision ([619792f](https://bitbucket.org/atlassian/atlaskit/commits/619792f))
* bug fix; code splitted avatar in mention and task-decision packages (issues closed: ed-2776) ([19f8276](https://bitbucket.org/atlassian/atlaskit/commits/19f8276))
## 4.0.1 (2017-09-18)

* bug fix; make sure box shadow for card style is rendered inside the enclosing container. (issues closed: fs-1382) ([4b441f9](https://bitbucket.org/atlassian/atlaskit/commits/4b441f9))
## 4.0.0 (2017-09-12)

* feature; support attribution label depending on state/creator/updaters. (issues closed: fs-1368) ([8955414](https://bitbucket.org/atlassian/atlaskit/commits/8955414))
* bug fix; fix content height in card appearance in IE11. ([e51886b](https://bitbucket.org/atlassian/atlaskit/commits/e51886b))
* breaking; Removed incomplete/unused ResourcedDecisionList and ResourcedTaskList. Changed values of appearance ([5aeb4f9](https://bitbucket.org/atlassian/atlaskit/commits/5aeb4f9))
* breaking; support participant rendering in action/decision cards (issues closed: fs-1307) ([5aeb4f9](https://bitbucket.org/atlassian/atlaskit/commits/5aeb4f9))
## 3.7.0 (2017-09-06)

* feature; add toolbar button support for actions/decisions (issues closed: fs-1342) ([faddb0b](https://bitbucket.org/atlassian/atlaskit/commits/faddb0b))
## 3.6.0 (2017-09-05)

* feature; visual updates based on action and decision designs. (issues closed: fs-1232) ([8b2126f](https://bitbucket.org/atlassian/atlaskit/commits/8b2126f))
## 3.5.5 (2017-09-01)

* bug fix; break and wrap text that doesn't fit on a full line (e.g. link) (issues closed: fs-1300) ([2f8d413](https://bitbucket.org/atlassian/atlaskit/commits/2f8d413))





## 3.5.4 (2017-08-29)

* bug fix; results clears if initialQuery prop changes. Consistent spinner. (issues closed: fs-1315) ([0933f72](https://bitbucket.org/atlassian/atlaskit/commits/0933f72))
## 3.5.3 (2017-08-25)

* bug fix; show placeholder with ellipsis it it will overflow (issues closed: fs-1286) ([d18ccba](https://bitbucket.org/atlassian/atlaskit/commits/d18ccba))
## 3.5.2 (2017-08-25)

* bug fix; direct import date-fns for support classes too (issues closed: fs-1280) ([0dfa9e8](https://bitbucket.org/atlassian/atlaskit/commits/0dfa9e8))
* bug fix; switch to lighter weight date-fns (issues closed: fs-1281) ([dd21922](https://bitbucket.org/atlassian/atlaskit/commits/dd21922))
## 3.5.1 (2017-08-23)

* bug fix; optimistically notify when a task state changes. (issues closed: fs-1285) ([d9a4557](https://bitbucket.org/atlassian/atlaskit/commits/d9a4557))
## 3.5.0 (2017-08-23)

* feature; support emptyComponent and errorComponent for ResourceItemList (issues closed: fs-1292) ([4b012e2](https://bitbucket.org/atlassian/atlaskit/commits/4b012e2))
## 3.4.1 (2017-08-18)

* bug fix; add missing dependency from InfiniteScroll ([e43126c](https://bitbucket.org/atlassian/atlaskit/commits/e43126c))
## 3.4.0 (2017-08-17)

* feature; support retry for recentUpdates if expecting item is not found. (issues closed: fs-1284) ([ed9af1e](https://bitbucket.org/atlassian/atlaskit/commits/ed9af1e))
## 3.3.1 (2017-08-17)

* bug fix; ensure RendererContext is passed to renderDocument prop when rendering a ResourcedIt (issues closed: fs-1282) ([ec2a02a](https://bitbucket.org/atlassian/atlaskit/commits/ec2a02a))
## 3.3.0 (2017-08-17)

* feature; support infinite scroll for ResourcedItemList (issues closed: fs-1268) ([a7bbfe2](https://bitbucket.org/atlassian/atlaskit/commits/a7bbfe2))
## 3.2.0 (2017-08-17)

* feature; support refreshing of ResourcedItemList and task state updates from an external tri (issues closed: fs-1267) ([bc2d4f1](https://bitbucket.org/atlassian/atlaskit/commits/bc2d4f1))
## 3.1.2 (2017-08-17)

* bug fix; fix exporting of support json data (issues closed: fs-1274) ([b1ec12f](https://bitbucket.org/atlassian/atlaskit/commits/b1ec12f))
## 3.1.1 (2017-08-16)

* bug fix; fix incorrect prop type for taskDecisionProvider - it should be a Promise<TaskDecisi (issues closed: fs-1274) ([6a11027](https://bitbucket.org/atlassian/atlaskit/commits/6a11027))
## 3.1.0 (2017-08-14)

* feature; make default query ordering CREATION_DATE (issues closed: fs-1259) ([96e546d](https://bitbucket.org/atlassian/atlaskit/commits/96e546d))

* bug fix; export ResourcedItemList ([4385f29](https://bitbucket.org/atlassian/atlaskit/commits/4385f29))
* feature; add support for grouping items by sort date in ResourcedItemList. (issues closed: fs-1259) ([dbff6cf](https://bitbucket.org/atlassian/atlaskit/commits/dbff6cf))
## 3.0.0 (2017-08-14)

* breaking; TaskDecisionProvider has new required methods. ([9e48cf4](https://bitbucket.org/atlassian/atlaskit/commits/9e48cf4))
* breaking; support service integration with tasks and all item types (issues closed: fs-1249) ([9e48cf4](https://bitbucket.org/atlassian/atlaskit/commits/9e48cf4))


## 2.4.0 (2017-08-09)

* feature; adding support for placeholders ([d9edd1a](https://bitbucket.org/atlassian/atlaskit/commits/d9edd1a))
## 2.3.0 (2017-08-09)



* feature; adding usupport to fetch initial state and toggle state ([416ce4e](https://bitbucket.org/atlassian/atlaskit/commits/416ce4e))
* feature; adding resourcedtaskitem ([1c8cccb](https://bitbucket.org/atlassian/atlaskit/commits/1c8cccb))
## 2.2.3 (2017-08-08)

* bug fix; import es5 renderer ([221da82](https://bitbucket.org/atlassian/atlaskit/commits/221da82))
## 2.2.2 (2017-08-07)

* bug fix; fix correct dep for @atlaskit/spinner ([155979d](https://bitbucket.org/atlassian/atlaskit/commits/155979d))
## 2.2.1 (2017-08-03)

* bug fix; fixes broken storybooks due to ED-2389 ([184d93a](https://bitbucket.org/atlassian/atlaskit/commits/184d93a))
## 2.2.0 (2017-08-02)

* bug fix; fix renderer dependency ([2ff20ff](https://bitbucket.org/atlassian/atlaskit/commits/2ff20ff))

* bug fix; fix type export for serviceDecision in test-data ([4ad5bac](https://bitbucket.org/atlassian/atlaskit/commits/4ad5bac))
* feature; add support for service integration for decisions (issues closed: fs-1187) ([6683f58](https://bitbucket.org/atlassian/atlaskit/commits/6683f58))
## 2.1.3 (2017-08-01)

* bug fix; using new renderer from editor-core ([32726cf](https://bitbucket.org/atlassian/atlaskit/commits/32726cf))




## 2.1.2 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 2.1.1 (2017-07-20)


* fix; fixes some IE11-bugs ([de3a2ce](https://bitbucket.org/atlassian/atlaskit/commits/de3a2ce))

## 2.1.0 (2017-07-20)


* feature; adding taskitem and tasklist ([7385442](https://bitbucket.org/atlassian/atlaskit/commits/7385442))

## 1.0.0 (2017-07-19)


* feature; new task-decision component. With decision components ([ea94187](https://bitbucket.org/atlassian/atlaskit/commits/ea94187))