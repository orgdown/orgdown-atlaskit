# @atlaskit/editor-bitbucket-transformer

## 2.6.5

## 2.6.4

## 2.6.3

## 2.6.2

## 2.6.1

## 2.6.0

## 2.5.6
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 2.5.5

## 2.5.4

## 2.5.3
- [patch] bump editor-common to 6.1.2 [bb7802e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bb7802e)

## 2.5.2
- [patch] bump mention to 9.1.1 to fix mention autocomplete bug [c7708c6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c7708c6)

## 2.5.1

## 2.5.0
- [minor] Fixed issue causing leading ' characters to be escaped [1b2140c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1b2140c)
- [patch] Fix images with underscore in URL [42ed524](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/42ed524)

## 2.4.2

## 2.4.1

## 2.4.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 2.3.1

## 2.3.0

## 2.2.0

## 2.1.12
- [patch] Fix dependencies in CI [670e930](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/670e930)

## 2.1.11

## 2.1.10

- [patch] Change to use editor-core instead of editor-bitbucket for examples [aa0c0ac](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/aa0c0ac)

## 2.1.9

## 2.1.8

## 2.1.7

## 2.1.6

## 2.1.5

## 2.1.4

## 2.1.3

## 2.1.2

## 2.1.1


- [patch] Change @atlaskit/editor-core to be only a dev dependency [765b8ff](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/765b8ff)
- [patch] Change @atlaskit/editor-core to be only a dev dependency [765b8ff](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/765b8ff)

## 2.1.0

## 2.0.5

## 2.0.4

## 2.0.3

## 2.0.2
- [patch] Fix of the build scripts for editor-*-transformer packages [59b4ea5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/59b4ea5)
- [patch] Fix of the build scripts for editor-*-transformer packages [59b4ea5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/59b4ea5)

## 2.0.1
- [patch] Fixed linting [5ebf75d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5ebf75d)
- [patch] Fixed linting [5ebf75d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5ebf75d)

## 2.0.0
- [major] Adding separate transformer packages. [f734c01](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f734c01)
- [major] Adding separate transformer packages. [f734c01](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f734c01)
