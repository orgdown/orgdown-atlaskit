# @atlaskit/editor-jira-transformer

## 2.4.13

## 2.4.12

## 2.4.11

## 2.4.10

## 2.4.9

## 2.4.8
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 2.4.7

## 2.4.6

## 2.4.5
- [patch] bump editor-common to 6.1.2 [bb7802e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bb7802e)

## 2.4.4
- [patch] bump mention to 9.1.1 to fix mention autocomplete bug [c7708c6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c7708c6)

## 2.4.3

## 2.4.2

## 2.4.1

## 2.4.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 2.3.2
- [patch] Fix transformation of list with empty text child and a sublist. [ad5441f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ad5441f)

## 2.3.1

## 2.3.0

## 2.2.1
- [patch] Adding missing dependency collapse-whitespace in editor-jira-transformer. [79a668e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/79a668e)

## 2.2.0

## 2.1.9

## 2.1.8

## 2.1.7

## 2.1.6

## 2.1.5

## 2.1.4

## 2.1.3

## 2.1.2

## 2.1.1

## 2.1.0

## 2.0.4

## 2.0.3

## 2.0.2

## 2.0.1
- [patch] Fix of the build scripts for editor-*-transformer packages [59b4ea5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/59b4ea5)
- [patch] Fix of the build scripts for editor-*-transformer packages [59b4ea5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/59b4ea5)

## 2.0.0
- [major] Adding separate transformer packages. [f734c01](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f734c01)
- [major] Adding separate transformer packages. [f734c01](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f734c01)
