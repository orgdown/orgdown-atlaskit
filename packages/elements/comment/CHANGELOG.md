# @atlaskit/comment

## 2.7.2
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 2.7.1

## 2.7.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 2.6.16

## 2.6.15
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 2.6.14
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 2.6.13

## 2.6.12
- [patch] Fix comment layout to prevent unmount on every change [fbf6db3](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/fbf6db3)

## 2.6.11

## 2.6.10

## 2.6.9
- [patch] Resolved low hanging flow errors in field-base field-text comment icon item and website, $ [007de27](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/007de27)

## 2.6.8
- [patch] migrated ak to mk-2 [c3d17da](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c3d17da)

## 2.6.7 (2017-10-26)

* bug fix; fix to rebuild stories ([793b2a7](https://bitbucket.org/atlassian/atlaskit/commits/793b2a7))

## 2.6.6 (2017-10-22)

* bug fix; update dependencies for react-16 ([077d1ad](https://bitbucket.org/atlassian/atlaskit/commits/077d1ad))

## 2.6.5 (2017-10-15)


* bug fix; update dependencies for react 16 compatibility ([fc47c94](https://bitbucket.org/atlassian/atlaskit/commits/fc47c94))

## 2.6.4 (2017-08-21)

* bug fix; fix PropTypes warning ([040d579](https://bitbucket.org/atlassian/atlaskit/commits/040d579))
## 2.6.3 (2017-08-11)

* bug fix; fix the theme-dependency ([db90333](https://bitbucket.org/atlassian/atlaskit/commits/db90333))








## 2.6.2 (2017-07-27)


* fix; rename jsnext:main to jsnext:experimental:main temporarily ([c7508e0](https://bitbucket.org/atlassian/atlaskit/commits/c7508e0))

## 2.6.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 2.3.0 (2017-07-17)

## 2.3.0 (2017-07-17)

## 2.3.0 (2017-07-17)


* fix; rerelease, failed prepublish scripts ([5fd82f8](https://bitbucket.org/atlassian/atlaskit/commits/5fd82f8))

## 2.3.0 (2017-07-17)


* feature; added ES module builds to dist and add jsnext:main to most ADG packages ([ea76507](https://bitbucket.org/atlassian/atlaskit/commits/ea76507))

## 2.2.0 (2017-07-04)

## 2.2.0 (2017-07-04)


* fix; remove timestamp prop from CommentTime. Create relative time usage example ([afb8fc0](https://bitbucket.org/atlassian/atlaskit/commits/afb8fc0))


* feature; add highlighted appearance state to comments ([683b905](https://bitbucket.org/atlassian/atlaskit/commits/683b905))

## 2.1.0 (2017-06-29)


* fix; update comment restricted icon to match design spec ([5f9a942](https://bitbucket.org/atlassian/atlaskit/commits/5f9a942))


* feature; commentTime accepts timestamp prop to display relative time. ([7711a22](https://bitbucket.org/atlassian/atlaskit/commits/7711a22))

## 2.0.1 (2017-06-22)


* fix; add usage pattern for comment with inline delete ([19426d4](https://bitbucket.org/atlassian/atlaskit/commits/19426d4))

## 2.0.0 (2017-06-14)


null refactor comment to styled-components ([204a35f](https://bitbucket.org/atlassian/atlaskit/commits/204a35f))


* breaking; Convert component to use styled-components instead of less

ISSUES CLOSED: #AK-2383

## 1.5.2 (2017-06-01)


* fix; fS-1018 When no other actions present, reactions button does not left-align with com ([85a539f](https://bitbucket.org/atlassian/atlaskit/commits/85a539f))

## 1.5.1 (2017-05-30)


* fix; fS-957 Center comment actions into flexbox ([b6968d4](https://bitbucket.org/atlassian/atlaskit/commits/b6968d4))
* fix; add prop-types as a dependency to avoid React 15.x warnings ([92598eb](https://bitbucket.org/atlassian/atlaskit/commits/92598eb))

## 1.5.0 (2017-05-02)


* feature; adds 'errorActions', 'isError' and 'errorIconLabel' props for showing error states ([4621fb3](https://bitbucket.org/atlassian/atlaskit/commits/4621fb3))
* feature; adds 'errorActions', 'isError' and 'errorIconLabel' props to ak-comment for showing ([de38d98](https://bitbucket.org/atlassian/atlaskit/commits/de38d98))

## 1.4.2 (2017-04-27)


* fix; update legal copy to be more clear. Not all modules include ADG license. ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 1.4.1 (2017-04-26)


* fix; update legal copy and fix broken links for component README on npm. New contribution and ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))

## 1.4.0 (2017-04-20)


* feature; removed explicit style! imports, set style-loader in webpack config ([891fc3c](https://bitbucket.org/atlassian/atlaskit/commits/891fc3c))

## 1.3.0 (2017-04-18)


* feature; updated avatar dependency versions for comment, dropdown-menu, droplist, and page ([e4d2ae7](https://bitbucket.org/atlassian/atlaskit/commits/e4d2ae7))

## 1.2.2 (2017-04-11)


* fix; update comment stories to use new readme component ([7d25b2f](https://bitbucket.org/atlassian/atlaskit/commits/7d25b2f))

## 1.2.1 (2017-04-04)


* fix; fix broken storybooks that use the editor component ([5c50bc3](https://bitbucket.org/atlassian/atlaskit/commits/5c50bc3))

## 1.2.0 (2017-03-27)


* feature; adds the isSaving and savingText props ([12a1f14](https://bitbucket.org/atlassian/atlaskit/commits/12a1f14))

## 1.1.1 (2017-03-23)

## 1.1.0 (2017-03-23)


* fix; Empty commit to release the component ([49c08ee](https://bitbucket.org/atlassian/atlaskit/commits/49c08ee))


* feature; adds restricted prop to Comments ([dd81d3b](https://bitbucket.org/atlassian/atlaskit/commits/dd81d3b))
* feature; renamed the restricted prop to "restrictedTo" for code clarity ([2f20eb6](https://bitbucket.org/atlassian/atlaskit/commits/2f20eb6))

## 1.0.5 (2017-03-21)

## 1.0.5 (2017-03-21)


* fix; maintainers for all the packages were added ([261d00a](https://bitbucket.org/atlassian/atlaskit/commits/261d00a))

## 1.0.4 (2017-03-16)


* fix; added flex-basis: 100% to fix ie11 ([c1e7e22](https://bitbucket.org/atlassian/atlaskit/commits/c1e7e22))
* fix; removes width: 100% from the comment content style, as this would allow the content ([eca5a31](https://bitbucket.org/atlassian/atlaskit/commits/eca5a31))

## 1.0.3 (2017-03-08)


* fix; fixes for alignment and spacing in the comment component ([cca7a03](https://bitbucket.org/atlassian/atlaskit/commits/cca7a03))

## 1.0.2 (2017-02-16)


* fix; Content no longer an array and instead a single node. ([6b6b2e1](https://bitbucket.org/atlassian/atlaskit/commits/6b6b2e1))

## 1.0.1 (2017-02-07)


* fix; Updates package to use ak scoped packages ([f80a01f](https://bitbucket.org/atlassian/atlaskit/commits/f80a01f))
