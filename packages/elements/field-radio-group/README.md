# RadioGroup

Provides a standard way to select a single option from a list.

## Installation

npm install @atlaskit/field-radio-group

## Usage

See [here](https://atlaskit.atlassian.com/packages/elements/field-radio-group)
