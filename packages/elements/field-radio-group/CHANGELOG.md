# @atlaskit/field-radio-group

## 1.9.2
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 1.9.1
- [patch] Update links in documentation [c4f7497](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c4f7497)

## 1.9.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 1.8.4
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 1.8.3
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 1.8.2


- [patch] Minor documentation fixes [f0e96bd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f0e96bd)

## 1.8.1
- [patch] Resolved low hanging flow errors in field-base field-text comment icon item and website, $ [007de27](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/007de27)

## 1.8.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 1.7.5 (2017-11-22)

* bug fix; removed focus styling from radio and checkbox svg as they will never be focused (issues closed: ak-3710) ([ec68128](https://bitbucket.org/atlassian/atlaskit/commits/ec68128))
* bug fix; checkbox and radio should not highlight when parent element is focused (issues closed: ak-3710) ([5c900ff](https://bitbucket.org/atlassian/atlaskit/commits/5c900ff))
## 1.7.4 (2017-11-21)

* bug fix; moving to fuzzy version for field-base dependency ([779b833](https://bitbucket.org/atlassian/atlaskit/commits/779b833))
## 1.7.3 (2017-10-26)

* bug fix; fix to rebuild stories ([793b2a7](https://bitbucket.org/atlassian/atlaskit/commits/793b2a7))

## 1.7.2 (2017-10-22)

* bug fix; update styled component dependency and react peerDep ([39f3286](https://bitbucket.org/atlassian/atlaskit/commits/39f3286))
## 1.7.1 (2017-10-16)

* bug fix; add back in removed label (issues closed: #ak-3649) ([75d197b](https://bitbucket.org/atlassian/atlaskit/commits/75d197b))
## 1.7.0 (2017-09-27)


* feature; add darkmode to radio group (issues closed: #ak3548) ([ec174b1](https://bitbucket.org/atlassian/atlaskit/commits/ec174b1))
## 1.6.4 (2017-09-13)

* bug fix; switch compontent to using theme over util-shared-styles ([bca2ebe](https://bitbucket.org/atlassian/atlaskit/commits/bca2ebe))
* bug fix; update dependencies ([fee4616](https://bitbucket.org/atlassian/atlaskit/commits/fee4616))


## 1.6.3 (2017-08-11)

* bug fix; fix the theme-dependency ([db90333](https://bitbucket.org/atlassian/atlaskit/commits/db90333))









## 1.6.2 (2017-07-27)


* fix; rename jsnext:main to jsnext:experimental:main temporarily ([c7508e0](https://bitbucket.org/atlassian/atlaskit/commits/c7508e0))

## 1.6.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 1.3.0 (2017-07-17)

## 1.3.0 (2017-07-17)

## 1.3.0 (2017-07-17)


* fix; rerelease, failed prepublish scripts ([5fd82f8](https://bitbucket.org/atlassian/atlaskit/commits/5fd82f8))

## 1.3.0 (2017-07-17)


* feature; added ES module builds to dist and add jsnext:main to most ADG packages ([ea76507](https://bitbucket.org/atlassian/atlaskit/commits/ea76507))

## 1.2.1 (2017-06-01)


* fix; add prop-types as a dependency to avoid React 15.x warnings ([92598eb](https://bitbucket.org/atlassian/atlaskit/commits/92598eb))
* fix; update dependencies in field-radio-group ([6f9223c](https://bitbucket.org/atlassian/atlaskit/commits/6f9223c))

## 1.2.0 (2017-05-08)


* fix; reverts the abiliity to call preventDefault during onRadioChange for smart component ([7a98842](https://bitbucket.org/atlassian/atlaskit/commits/7a98842))


* feature; adds onRadioChange to smart field-radio-group (allows preventDefault to prevent cha ([1862bb8](https://bitbucket.org/atlassian/atlaskit/commits/1862bb8))

## 1.1.2 (2017-04-27)


* fix; update legal copy to be more clear. Not all modules include ADG license. ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 1.1.1 (2017-04-26)


* fix; update legal copy and fix broken links for component README on npm. New contribution and ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))

## 1.1.0 (2017-04-20)


* feature; removed explicit style! imports, set style-loader in webpack config ([891fc3c](https://bitbucket.org/atlassian/atlaskit/commits/891fc3c))

## 1.0.6 (2017-03-23)


* fix; Empty commit to release the component ([49c08ee](https://bitbucket.org/atlassian/atlaskit/commits/49c08ee))

## 1.0.4 (2017-03-21)

## 1.0.4 (2017-03-21)


* fix; maintainers for all the packages were added ([261d00a](https://bitbucket.org/atlassian/atlaskit/commits/261d00a))

## 1.0.3 (2017-02-16)


* fix; refactor stories to use // rather than http:// ([a0826cf](https://bitbucket.org/atlassian/atlaskit/commits/a0826cf))

## 1.0.2 (2017-02-10)


* fix; Dummy commit to release components to registry ([5bac43b](https://bitbucket.org/atlassian/atlaskit/commits/5bac43b))

## 1.0.1 (2017-02-06)


* fix; Updates package to use scoped ak packages ([d6bca89](https://bitbucket.org/atlassian/atlaskit/commits/d6bca89))
