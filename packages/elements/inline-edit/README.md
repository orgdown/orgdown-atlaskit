# Inline Edit

A component for displaying and editing values in fields.

## Installation

npm install @atlaskit/inline-edit

## Usage

See [here](https://atlaskit.atlassian.com/packages/elements/inline-edit)
