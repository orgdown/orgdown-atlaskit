# @atlaskit/lozenge

## 3.6.1
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 3.6.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 3.5.2
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 3.5.1
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 3.5.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 3.4.10

## 3.4.9

## 3.4.8

## 3.4.7

## 3.4.6

## Unreleased

## 3.4.2 (2017-07-27)

* fix; rename jsnext:main to jsnext:experimental:main temporarily ([c7508e0](https://bitbucket.org/atlassian/atlaskit/commits/c7508e0))

## 3.4.1 (2017-07-25)

* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 3.1.0 (2017-07-17)

## 3.1.0 (2017-07-17)

## 3.1.0 (2017-07-17)

* fix; rerelease, failed prepublish scripts ([5fd82f8](https://bitbucket.org/atlassian/atlaskit/commits/5fd82f8))

## 3.1.0 (2017-07-17)

* feature; added ES module builds to dist and add jsnext:main to most ADG packages ([ea76507](https://bitbucket.org/atlassian/atlaskit/commits/ea76507))

## 3.0.3 (2017-07-13)

* fix; add prop-types as a dependency to avoid React 15.x warnings ([92598eb](https://bitbucket.org/atlassian/atlaskit/commits/92598eb))

## 3.0.2 (2017-04-27)

* fix; update legal copy to be more clear. Not all modules include ADG license. ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 3.0.1 (2017-04-26)

* fix; update legal copy and fix broken links for component README on npm. New contribution and ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))

## 2.0.0 (2017-03-27)

null bump major to avoid API conflict with mentions ([1d01253](https://bitbucket.org/atlassian/atlaskit/commits/1d01253))
null updating dependencies ([d293404](https://bitbucket.org/atlassian/atlaskit/commits/d293404))

* breaking; update lozenge major version
* breaking; removed classnames dep and .d.ts file

## 1.0.11 (2017-03-23)

* fix; Empty commit to release the component ([49c08ee](https://bitbucket.org/atlassian/atlaskit/commits/49c08ee))

null refactor the lozenge component to use styled-components ([eb738ca](https://bitbucket.org/atlassian/atlaskit/commits/eb738ca))

* breaking; now requires peerDep of "styled-components"

## 1.0.9 (2017-03-21)

## 1.0.9 (2017-03-21)

* fix; maintainers for all the packages were added ([261d00a](https://bitbucket.org/atlassian/atlaskit/commits/261d00a))

## 1.0.8 (2017-02-28)

* fix; dummy commit to release stories ([3df5d9f](https://bitbucket.org/atlassian/atlaskit/commits/3df5d9f))

## 1.0.6 (2017-02-28)

* fix; dummy commit to fix broken stories and missing registry pages ([a31e92a](https://bitbucket.org/atlassian/atlaskit/commits/a31e92a))

## 1.0.6 (2017-02-28)

* fix; dummy commit to release stories for components ([a105c02](https://bitbucket.org/atlassian/atlaskit/commits/a105c02))

## 1.0.5 (2017-02-28)

* fix; removes jsdoc annotations and moves them to usage.md ([2c53fea](https://bitbucket.org/atlassian/atlaskit/commits/2c53fea))

## 1.0.4 (2017-02-20)

* fix; use correctly scoped package names in npm docs ([91dbd2f](https://bitbucket.org/atlassian/atlaskit/commits/91dbd2f))

## 1.0.3 (2017-02-07)

## 1.0.2 (2017-02-07)

* fix; Updates docs with yarn installation instructions ([aebf31a](https://bitbucket.org/atlassian/atlaskit/commits/aebf31a))
* Add TypeScript declarations for lozenge. ([5993cf8](https://bitbucket.org/atlassian/atlaskit/commits/5993cf8))

## 1.0.1 (2017-02-06)

* fix; Updates package to use scoped ak packages ([b655c30](https://bitbucket.org/atlassian/atlaskit/commits/b655c30))
