# @atlaskit/polyfills

This package contains @atlaskit/polyfills for native methods which are unsupported by some of the browsers that we target.

## Try it out

Interact with a [live demo of the @atlaskit/polyfills component](https://atlaskit.atlassian.com).

## Installation

```sh
npm install @atlaskit/polyfills
```
