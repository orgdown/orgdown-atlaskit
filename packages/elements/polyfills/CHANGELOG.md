# @atlaskit/polyfills

## 1.4.3
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 1.4.2
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 1.4.1

- [patch] Mark packages as internal [016d74d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/016d74d)

## 1.4.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 1.3.0
- [minor] added array.prototype.find polyfull to package [d1eb5b7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d1eb5b7)

## 1.2.4

## 1.2.3
- [patch] Removes broken dist builds and simply exposes individual polyfills [c9e83f6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c9e83f6)

## 1.2.2
- [patch] Added *.js glob to files property in package.json, moved polyfills to root [96f25cc](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/96f25cc)

## 1.2.1
- [patch] bump editor-docs to 1.0.0 in polifills package [7629794](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7629794)
- [patch] Moved to new repo... [f81334e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f81334e)
