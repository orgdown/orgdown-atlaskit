# Page

Provides Grid layouts for content & components

## Installation

npm install @atlaskit/page

## Usage

See [here](https://atlaskit.atlassian.com/packages/elements/page)
