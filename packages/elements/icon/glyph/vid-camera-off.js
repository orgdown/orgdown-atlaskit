'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var VidCameraOffIcon = function VidCameraOffIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><g fill="currentColor" fill-rule="evenodd"><path d="M17 9.828v5.29l3.53 1.77c.152.077.313.112.47.112.523 0 1-.395 1-.94V7.94c0-.545-.477-.94-1-.94-.157 0-.317.035-.469.11l-1.625.812L17 9.828zm-2.062-3.595l-11.64 11.64A2.003 2.003 0 0 1 2 15.999V8.003A2 2 0 0 1 3.999 6H14c.339 0 .658.085.937.233zM16 10.828v5.17A2 2 0 0 1 14.001 18H8.828L16 10.828z"/><path d="M3.793 20.207a.996.996 0 0 0 1.412.002L20.21 5.205a.996.996 0 0 0-.002-1.412.996.996 0 0 0-1.412-.002L3.79 18.795a.996.996 0 0 0 .002 1.412z" fill-rule="nonzero"/></g></svg>' }, props));
};
exports.default = VidCameraOffIcon;