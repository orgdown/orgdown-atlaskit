'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects24FavoriteIcon = function Objects24FavoriteIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M3 0h18a3 3 0 0 1 3 3v18a3 3 0 0 1-3 3H3a3 3 0 0 1-3-3V3a3 3 0 0 1 3-3zm9 17.514l3.498 1.928a1 1 0 0 0 1.47-1.037l-.68-4.156 2.923-2.986a1 1 0 0 0-.564-1.688l-3.997-.609-1.745-3.706a1 1 0 0 0-1.81 0L9.35 8.966l-3.997.61a1 1 0 0 0-.564 1.687l2.923 2.986-.68 4.156a1 1 0 0 0 1.47 1.037L12 17.514z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects24FavoriteIcon;