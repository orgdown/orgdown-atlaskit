'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects24ActionIcon = function Objects24ActionIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M3 0h18a3 3 0 0 1 3 3v18a3 3 0 0 1-3 3H3a3 3 0 0 1-3-3V3a3 3 0 0 1 3-3zm1 5v14c0 .555.448 1 1 1h14c.555 0 1-.448 1-1V5c0-.555-.448-1-1-1H5c-.555 0-1 .448-1 1zm7.038 7.5l3.261-3.118a1 1 0 1 1 1.383 1.445l-3.964 3.791a1 1 0 0 1-1.394-.011l-1.997-1.973a1 1 0 0 1 1.405-1.423l1.306 1.29z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects24ActionIcon;