'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects24SupportIcon = function Objects24SupportIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M3 0h18a3 3 0 0 1 3 3v18a3 3 0 0 1-3 3H3a3 3 0 0 1-3-3V3a3 3 0 0 1 3-3zm16.973 10.4c.107-1.25-.099-2.426-.773-3.493a3.875 3.875 0 0 0-4.124-1.82 4.696 4.696 0 0 0-2.529 1.425c-.192.198-.364.415-.545.622a.128.128 0 0 1-.039-.016c-.041-.051-.083-.1-.123-.152a5.357 5.357 0 0 0-1.805-1.5c-1.57-.794-3.336-.567-4.577.632C4.403 7.116 4 8.411 4 9.865a6.333 6.333 0 0 0 1.013 3.375 16.729 16.729 0 0 0 3.222 3.786 9.856 9.856 0 0 0 2.42 1.631c.51.23 1.032.398 1.6.326.732-.094 1.376-.419 1.985-.815 1.783-1.16 3.224-2.679 4.432-4.436.688-1.005 1.194-2.088 1.301-3.332z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects24SupportIcon;