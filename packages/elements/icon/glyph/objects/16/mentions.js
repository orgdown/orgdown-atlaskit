'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects16MentionsIcon = function Objects16MentionsIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M6 4h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2zm7.849 5.085a.866.866 0 0 0-.765.46 2.022 2.022 0 0 0-1.326-.471c-1.465 0-2.473 1.192-2.473 2.88 0 1.76 1.007 2.972 2.512 2.972.621 0 1.136-.22 1.494-.631.391.632 1.116 1.042 1.79 1.042 1.597 0 2.921-1.67 2.921-3.947 0-3.122-2.536-5.386-6.002-5.386v.25-.25c-3.363 0-6.002 2.638-6.002 6.002 0 3.616 2.67 6.002 6.002 6.002.741 0 1.371-.078 1.926-.24a.868.868 0 0 0-.483-1.664c-.394.115-.865.171-1.443.171-2.4 0-4.27-1.617-4.27-4.27 0-2.395 1.874-4.269 4.27-4.269 2.53 0 4.27 1.502 4.27 3.654 0 1.342-.617 2.215-1.188 2.215-.113 0-.367-.146-.367-.367V9.952a.866.866 0 0 0-.866-.867zm-1.798 4.507v-.25c.597 0 .932-.46.932-1.336 0-.89-.335-1.367-.919-1.367-.604 0-.955.47-.955 1.354 0 .871.356 1.349.942 1.349v.25z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects16MentionsIcon;