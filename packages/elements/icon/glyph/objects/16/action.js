'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects16ActionIcon = function Objects16ActionIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M4 5.994C4 4.893 4.895 4 5.994 4h12.012C19.107 4 20 4.895 20 5.994v12.012A1.995 1.995 0 0 1 18.006 20H5.994A1.995 1.995 0 0 1 4 18.006V5.994zM8.667 7C7.747 7 7 7.746 7 8.667v6.666C7 16.253 7.746 17 8.667 17h6.666c.92 0 1.667-.746 1.667-1.667V8.667C17 7.747 16.254 7 15.333 7H8.667zm4.662 3.277a1 1 0 0 1 1.382 1.446l-2.673 2.556a1 1 0 0 1-1.394-.011l-1.347-1.33a1 1 0 1 1 1.406-1.423l.655.647 1.97-1.885z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects16ActionIcon;