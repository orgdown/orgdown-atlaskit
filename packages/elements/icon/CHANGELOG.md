# @atlaskit/icon

## 10.12.2
- [patch] add horizontal rule toolbar item [48c36f4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/48c36f4)

## 10.12.1
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 10.12.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 10.11.0
- [minor] Create skeleton representations of various components [cd628e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/cd628e4)

## 10.10.1
- [patch] update atlaskit src for internal consumption [4601bf0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4601bf0)

## 10.10.0
- [minor] Added 42 new icons for Objects [e00ff79](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e00ff79)

## 10.9.3
- [patch] removed role props to make it more accessible [88cc276](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/88cc276)

## 10.9.2
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 10.9.1
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 10.9.0
- [minor] New emoji-add icon [b8b1b51](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b8b1b51)

## 10.8.0
- [minor] added editor/success icon, updated a few other editor icons [911074c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/911074c)

## 10.7.1
- [patch] Resolved low hanging flow errors in field-base field-text comment icon item and website, $ [007de27](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/007de27)

## 10.7.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 10.6.0
- [minor] Updated switcher icon [2815441](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/2815441)

## 10.5.0
- [minor] Move icon and reduced-ui pack to new repo, update build process [b3977f3](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b3977f3)

## 10.4.0 (2017-12-08)

* feature; added new media layout icons for the editor (issues closed: ak-4012)
  ([ee770f5](https://bitbucket.org/atlassian/atlaskit/commits/ee770f5))

## 10.3.3 (2017-12-05)

* bug fix; fix product icon gradients not appearing with default icon colour
  ([013f52a](https://bitbucket.org/atlassian/atlaskit/commits/013f52a))
* bug fix; fix icon gradients not applying properly in safari in some cases
  (issues closed: ak-3744)
  ([e35edf8](https://bitbucket.org/atlassian/atlaskit/commits/e35edf8))

## 10.3.2 (2017-11-23)

* bug fix; remove theme package direct references
  ([0f99302](https://bitbucket.org/atlassian/atlaskit/commits/0f99302))

## 10.3.1 (2017-11-20)

* bug fix; fS-3907 Use content attribute instead of description for Tooltip
  ([25c9604](https://bitbucket.org/atlassian/atlaskit/commits/25c9604))
* bug fix; fS-3907 Bump tooltip version in icon, item and util-shared-styles
  ([6d20540](https://bitbucket.org/atlassian/atlaskit/commits/6d20540))

## 10.3.0 (2017-11-16)

* feature; new and updated icons for the editor (issues closed: ak-3720)
  ([2c709e2](https://bitbucket.org/atlassian/atlaskit/commits/2c709e2))

## 10.2.0 (2017-11-15)

* feature; added a new prop for icons to make them more performant.
  ([7dc38f7](https://bitbucket.org/atlassian/atlaskit/commits/7dc38f7))

* feature; icon component performance improved and the glyph can now be passed
  as a string.
  ([317274c](https://bitbucket.org/atlassian/atlaskit/commits/317274c))

## 10.1.3 (2017-11-09)

* bug fix; add missing color props
  ([f186c02](https://bitbucket.org/atlassian/atlaskit/commits/f186c02))

## 10.1.2 (2017-10-26)

* bug fix; fix to rebuild stories
  ([793b2a7](https://bitbucket.org/atlassian/atlaskit/commits/793b2a7))

## 10.1.1 (2017-10-22)

* bug fix; update styled component dependency and react peerDep
  ([39f3286](https://bitbucket.org/atlassian/atlaskit/commits/39f3286))

## 10.1.0 (2017-10-10)

* feature; added 8 new icons, updated 4 others (issues closed: ak-3590)
  ([0cff900](https://bitbucket.org/atlassian/atlaskit/commits/0cff900))

## 10.0.0 (2017-09-25)

* breaking; Removing the "editor/expand" icon. Use the appropriate
  chevron-up/chevron-down icons instead.
  ([dc2f175](https://bitbucket.org/atlassian/atlaskit/commits/dc2f175))
* breaking; removing the "expand" icon in preference to using the chevron ones
  instead (issues closed: ak-2157)
  ([dc2f175](https://bitbucket.org/atlassian/atlaskit/commits/dc2f175))

## 9.0.1 (2017-09-15)

* bug fix; removed glitched path from the People icon (issues closed: ak-3524)
  ([bb1da8a](https://bitbucket.org/atlassian/atlaskit/commits/bb1da8a))

## 9.0.0 (2017-09-11)

* breaking; The company/product icons (AtlassianIcon, BitbucketIcon,
  ConfluenceIcon, HipchatIcon, JiraIcon) have
  ([8a502b1](https://bitbucket.org/atlassian/atlaskit/commits/8a502b1))
* breaking; new company and product icons added
  ([8a502b1](https://bitbucket.org/atlassian/atlaskit/commits/8a502b1))

## 8.1.0 (2017-08-28)

* feature; added switcher icon back
  ([de848a6](https://bitbucket.org/atlassian/atlaskit/commits/de848a6))

## 8.0.1 (2017-08-21)

* bug fix; fix PropTypes warning
  ([040d579](https://bitbucket.org/atlassian/atlaskit/commits/040d579))

## 8.0.0 (2017-08-17)

* bug fix; fixing the devDep for icon on itself
  ([b3db177](https://bitbucket.org/atlassian/atlaskit/commits/b3db177))
* bug fix; fixed icon build script
  ([10aea52](https://bitbucket.org/atlassian/atlaskit/commits/10aea52))

- feature; updated stories for icons and updated the build step for
  reduced-ui-pack icons
  ([0ad9eea](https://bitbucket.org/atlassian/atlaskit/commits/0ad9eea))

- breaking; Some icons have been deleted, and some are now 2-colours
  ([733dbd3](https://bitbucket.org/atlassian/atlaskit/commits/733dbd3))
- breaking; icon audit and improvement
  ([733dbd3](https://bitbucket.org/atlassian/atlaskit/commits/733dbd3))

## 7.1.0 (2017-08-11)

* bug fix; make theme import absolute
  ([5ef8926](https://bitbucket.org/atlassian/atlaskit/commits/5ef8926))
* feature; support dark mode
  ([6701273](https://bitbucket.org/atlassian/atlaskit/commits/6701273))

## 7.0.3 (2017-07-25)

## 7.0.2 (2017-07-24)

* fix; use class transform in loose mode in babel to improve load performance in
  apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))
* fix; icons no longer grow massively in Webkit or when font size bumped
  ([adfb57e](https://bitbucket.org/atlassian/atlaskit/commits/adfb57e))

## 7.0.1 (2017-06-28)

* fix; changed icon wrapper display from inline-flex to inline-block
  ([64dc3de](https://bitbucket.org/atlassian/atlaskit/commits/64dc3de))

## 7.0.0 (2017-06-08)

* fix; refactored icon module and build process
  ([a68abba](https://bitbucket.org/atlassian/atlaskit/commits/a68abba))

- breaking; Module no longer exports named exports. Import a specific icon like
  so: import AtlassianIcon from '@atlaskit/icon/glyph/atlassian';

ISSUES CLOSED: AK-2388

## 6.6.0 (2017-05-31)

* fix; add prop-types as a dependency to avoid React 15.x warnings
  ([92598eb](https://bitbucket.org/atlassian/atlaskit/commits/92598eb))

- feature; added log-in icon to [@atlaskit](https://github.com/atlaskit)/icon
  and [@atlaskit](https://github.com/atlaskit)/reduced-ui-pack
  ([aa72586](https://bitbucket.org/atlassian/atlaskit/commits/aa72586))

## 6.5.4 (2017-05-12)

* fix; add package name and version to the hashing of classnames in
  [@atlaskit](https://github.com/atlaskit)/icon to preve
  ([a28bfe5](https://bitbucket.org/atlassian/atlaskit/commits/a28bfe5))

## 6.5.3 (2017-05-10)

* fix; do not import whole icon in media-card
  ([cc5ec63](https://bitbucket.org/atlassian/atlaskit/commits/cc5ec63))

## 6.5.2 (2017-04-27)

* fix; update legal copy to be more clear. Not all modules include ADG license.
  ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 6.5.1 (2017-04-26)

## 6.5.0 (2017-04-26)

## 6.4.0 (2017-04-26)

* fix; fixed bug in the new ExampleWithCode story type
  ([f30a4d3](https://bitbucket.org/atlassian/atlaskit/commits/f30a4d3))
* fix; checkbox icon now correctly a 2-colour icon again
  ([470642e](https://bitbucket.org/atlassian/atlaskit/commits/470642e))
* fix; icon SVG files updated. Fills made to be transparent for 2-color icons,
  and some ico
  ([264bb97](https://bitbucket.org/atlassian/atlaskit/commits/264bb97))
* fix; update legal copy and fix broken links for component README on npm. New
  contribution and
  ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))

- feature; Adds switcher icon
  ([220cc33](https://bitbucket.org/atlassian/atlaskit/commits/220cc33))
- feature; icons now have a primaryColor and secondaryColor prop
  ([9768e00](https://bitbucket.org/atlassian/atlaskit/commits/9768e00))

## 6.3.2 (2017-04-24)

* fix; fixing improper PropType for a prop 'glyph'
  ([cff41c5](https://bitbucket.org/atlassian/atlaskit/commits/cff41c5))

## 6.3.1 (2017-04-20)

* fix; fixes regressions where styles werent being loaded causing sizing to be
  broken ([1de6d0c](https://bitbucket.org/atlassian/atlaskit/commits/1de6d0c))

## 6.3.0 (2017-04-20)

* feature; removed explicit style! imports, set style-loader in webpack config
  ([891fc3c](https://bitbucket.org/atlassian/atlaskit/commits/891fc3c))

## 6.2.0 (2017-04-19)

## 6.1.0 (2017-04-19)

* feature; refactoring Icon to reduce bundle size and code complexity
  ([43c61f5](https://bitbucket.org/atlassian/atlaskit/commits/43c61f5))

## 6.0.1 (2017-04-18)

* fix; update icon stories to use new readme component
  ([1cdfa6d](https://bitbucket.org/atlassian/atlaskit/commits/1cdfa6d))

- feature; add media services scale large and small icons
  ([3bd9d86](https://bitbucket.org/atlassian/atlaskit/commits/3bd9d86))

## 5.0.0 (2017-03-28)

## 5.0.0 (2017-03-28)

* fix; remove icon baseline alignment story
  ([876c682](https://bitbucket.org/atlassian/atlaskit/commits/876c682))
* fix; use new 24px artboard size in 'too big' story
  ([404e6e0](https://bitbucket.org/atlassian/atlaskit/commits/404e6e0))

- feature; bulk icon update
  ([76367b5](https://bitbucket.org/atlassian/atlaskit/commits/76367b5))
- feature; update default icon sizes
  ([90850bd](https://bitbucket.org/atlassian/atlaskit/commits/90850bd))

* breaking; default SVG artboard sizes are now 24px, with some icons such as
  editor on the 16px artboard.
* breaking; icons are no longer guaranteed to baseline-align with sibling
  content. use flexbox to control

alignment.

ISSUES CLOSED: AK-1924

* breaking; This icon released contains a large amount of breaking naming
  changes due to deletions and renames.

Please update to this new major version and ensure your application is using the
correct icon

exports via linting.

ISSUES CLOSED: AK-1924

## 4.0.0 (2017-03-27)

## 3.0.3 (2017-03-23)

* fix; Empty commit to release the component
  ([49c08ee](https://bitbucket.org/atlassian/atlaskit/commits/49c08ee))

- feature; icon sizes changed to be on grid
  ([f6748ea](https://bitbucket.org/atlassian/atlaskit/commits/f6748ea))

* breaking; Icon sizes changed. This does not change the default sizes, only the
  ones when there is a size prop

specified.

ISSUES CLOSED: AK-1515

## 3.0.1 (2017-03-21)

## 3.0.1 (2017-03-21)

* fix; maintainers for all the packages were added
  ([261d00a](https://bitbucket.org/atlassian/atlaskit/commits/261d00a))

## 3.0.0 (2017-03-03)

* feature; move service desk icons out of the servicedesk directory
  ([f959e6b](https://bitbucket.org/atlassian/atlaskit/commits/f959e6b))

- breaking; Service desk icons now live in the root icons directory

ISSUES CLOSED: AK-1858

## 2.5.5 (2017-02-28)

* fix; dummy commit to release stories
  ([3df5d9f](https://bitbucket.org/atlassian/atlaskit/commits/3df5d9f))

## 2.5.3 (2017-02-28)

* fix; dummy commit to fix broken stories and missing registry pages
  ([a31e92a](https://bitbucket.org/atlassian/atlaskit/commits/a31e92a))

## 2.5.3 (2017-02-28)

* fix; dummy commit to release stories for components
  ([a105c02](https://bitbucket.org/atlassian/atlaskit/commits/a105c02))

## 2.5.2 (2017-02-28)

* fix; removes jsdoc annotations from icons and removes scripts used to generate
  Icons.md ([e5302a0](https://bitbucket.org/atlassian/atlaskit/commits/e5302a0))

## 2.5.1 (2017-02-27)

* fix; update flag's icon dependency to latest
  ([32885ce](https://bitbucket.org/atlassian/atlaskit/commits/32885ce))

## 2.5.0 (2017-02-21)

## 2.4.3 (2017-02-20)

* fix; change directory icon specific names to generic icon names
  ([13bb38a](https://bitbucket.org/atlassian/atlaskit/commits/13bb38a))

## 2.4.2 (2017-02-20)

## 2.4.1 (2017-02-20)

* fix; fix fill color typo in jira/addon icon
  ([8900095](https://bitbucket.org/atlassian/atlaskit/commits/8900095))
* fix; copy in-code comment about reduced-ui-pack to readme
  ([24e2c37](https://bitbucket.org/atlassian/atlaskit/commits/24e2c37))
* fix; use correctly scoped package names in npm docs
  ([91dbd2f](https://bitbucket.org/atlassian/atlaskit/commits/91dbd2f))

- feature; add icons for directory privacy levels
  ([fb5f89b](https://bitbucket.org/atlassian/atlaskit/commits/fb5f89b))

## 2.4.0 (2017-02-16)

* feature; Force icons version bump to get mediakit icons
  ([64bf24e](https://bitbucket.org/atlassian/atlaskit/commits/64bf24e))

## 2.3.3 (2017-02-07)

* fix; Rearrange tsconfig.json organisation to allow per-package configuration.
  ([6c6992d](https://bitbucket.org/atlassian/atlaskit/commits/6c6992d))

## 2.3.2 (2017-02-06)

## 2.3.1 (2017-02-06)

* fix; Updates packages to use scoped ak packages
  ([26285cb](https://bitbucket.org/atlassian/atlaskit/commits/26285cb))
* fix; Fixes invite team icon for icons
  ([3b66fd7](https://bitbucket.org/atlassian/atlaskit/commits/3b66fd7))
