# Droplist

This is a base (internal) component which should be used to build all the 'dropdown' lists.

## Installation

npm install @atlaskit/droplist

## Usage

See [here](https://atlaskit.atlassian.com/packages/elements/droplist)
