# @atlaskit/field-text-area

## 1.2.1
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 1.2.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 1.1.5
- [patch] More specific onChange event types in field-text-area [94c93eb](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/94c93eb)

## 1.1.4
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 1.1.3
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 1.1.2
- [patch] Change incorrect type info [ce915ea](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ce915ea)

## 1.1.1
- [patch] Resolved low hanging flow errors in field-base field-text comment icon item and website, $ [007de27](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/007de27)

## 1.1.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 1.0.5
- [patch] Fix version ranges on button/layer-manager [7e7a211](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7e7a211)

## 1.0.4
- [patch] Migrated package to atlaskit-mk-2 [a1950a9](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a1950a9)

## 1.0.3 (2017-11-21)

* bug fix; bumping internal dependencies to latest major versions ([aeebf29](https://bitbucket.org/atlassian/atlaskit/commits/aeebf29))
## 1.0.2 (2017-10-27)

* bug fix; rebuild stories ([7aa7337](https://bitbucket.org/atlassian/atlaskit/commits/7aa7337))


## 1.0.1 (2017-10-22)

* bug fix; update styled component dependency and react peerDep ([39f3286](https://bitbucket.org/atlassian/atlaskit/commits/39f3286))
## 1.0.0 (2017-10-20)

* breaking; Random break to bump to v1 ([d5ebc2e](https://bitbucket.org/atlassian/atlaskit/commits/d5ebc2e))
* breaking; update references to field-text ([d5ebc2e](https://bitbucket.org/atlassian/atlaskit/commits/d5ebc2e))