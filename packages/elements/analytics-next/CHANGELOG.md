# @atlaskit/analytics-next

## 1.1.2
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 1.1.1
- [patch] Remove min requirement of node 8 for analytics-next [c864671](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c864671)

## 1.1.0
- [minor] adds createAndFireEvent utility method and updates docs [24a93fc](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/24a93fc)

## 1.0.3
- [patch] fixes flow type problem with wrapping stateless functional components in withAnalyticsEvents [8344ffb](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8344ffb)

## 1.0.2
- [patch] Adds action key to analytics payload type [7deeaef](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7deeaef)

## 1.0.1
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 1.0.0
- [major] release @atlaskit/analytics-next package [80695ea](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/80695ea)
