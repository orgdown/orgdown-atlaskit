# @atlaskit/dynamic-table

## 7.2.1
- [patch] Re-releasing due to potentially broken babel release [9ed0bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9ed0bba)

## 7.2.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 7.1.2
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 7.1.1
- [patch] Packages Flow types for elements components [3111e74](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3111e74)

## 7.1.0
- [minor] Added possibility to drag and drop rows in dynamic table. [e69e6f4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e69e6f4)

## 7.0.1
- [patch] package bump to resolve discrepencies with npm [be745da](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/be745da)

## 6.2.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 6.1.5
- [patch] Fix manual bump bug from migration [fefd96c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/fefd96c)

## 7.0.0
- There were no major changes in this release.

## 6.1.4
- [patch] fix examples so that name is truly sortable, resolve prop error, fix rowCellType key prop type [e5981fb](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e5981fb)

## 6.1.3
- [patch] Migrate dynamic-table to ak-mk-2 repo  [8402863](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8402863)

## 6.1.2 (2017-12-07)

* bug fix; add story to demonstrate toggling pagination (issues closed: ak-3052) ([3f66ce8](https://bitbucket.org/atlassian/atlaskit/commits/3f66ce8))

## 6.1.1 (2017-11-15)

* bug fix; bumping internal dependencies to the latest major versions ([dd59c0a](https://bitbucket.org/atlassian/atlaskit/commits/dd59c0a))
## 6.1.0 (2017-11-13)












* feature; add a loading state to the dynamic table ([08974ae](https://bitbucket.org/atlassian/atlaskit/commits/08974ae))
## 6.0.2 (2017-10-26)

* bug fix; fix to rebuild stories ([793b2a7](https://bitbucket.org/atlassian/atlaskit/commits/793b2a7))
## 6.0.1 (2017-10-22)

* bug fix; update dependencies for react-16 ([077d1ad](https://bitbucket.org/atlassian/atlaskit/commits/077d1ad))
## 6.0.0 (2017-08-11)

* bug fix; fix the theme-dependency ([db90333](https://bitbucket.org/atlassian/atlaskit/commits/db90333))


* breaking; affects internal styled-components implementation ([d14522a](https://bitbucket.org/atlassian/atlaskit/commits/d14522a))
* breaking; implement dark mode theme ([d14522a](https://bitbucket.org/atlassian/atlaskit/commits/d14522a))








## 5.0.0 (2017-08-11)


* breaking; affects internal styled-components implementation ([d14522a](https://bitbucket.org/atlassian/atlaskit/commits/d14522a))
* breaking; implement dark mode theme ([d14522a](https://bitbucket.org/atlassian/atlaskit/commits/d14522a))









## 4.5.2 (2017-07-27)


* fix; rename jsnext:main to jsnext:experimental:main temporarily ([c7508e0](https://bitbucket.org/atlassian/atlaskit/commits/c7508e0))

## 4.5.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 4.2.0 (2017-07-17)

## 4.2.0 (2017-07-17)

## 4.2.0 (2017-07-17)


* fix; rerelease, failed prepublish scripts ([5fd82f8](https://bitbucket.org/atlassian/atlaskit/commits/5fd82f8))

## 4.2.0 (2017-07-17)


* feature; added ES module builds to dist and add jsnext:main to most ADG packages ([ea76507](https://bitbucket.org/atlassian/atlaskit/commits/ea76507))

## 4.1.0 (2017-07-12)


* fix; removes rowIndex prop from body render method ([9394455](https://bitbucket.org/atlassian/atlaskit/commits/9394455))

## 4.0.1 (2017-07-10)


* fix; fixes '0' being displayed when rows prop is empty array ([8ce1453](https://bitbucket.org/atlassian/atlaskit/commits/8ce1453))
* fix; fixes react console error by adding key prop to TableRow in render method of Body ([c2e08cb](https://bitbucket.org/atlassian/atlaskit/commits/c2e08cb))


* feature; updates pagination dependency to 3.1.0 to get ellipsis when there are lots of pages ([391db73](https://bitbucket.org/atlassian/atlaskit/commits/391db73))

## 4.0.0 (2017-07-03)


* fix; updates dynamic-table to always display table header ([ee35148](https://bitbucket.org/atlassian/atlaskit/commits/ee35148))
* fix; updates dynamic-table to have headerless emptyView ([aa87745](https://bitbucket.org/atlassian/atlaskit/commits/aa87745))
* fix; updates tests to check against Component not string ([a91743b](https://bitbucket.org/atlassian/atlaskit/commits/a91743b))
* fix; add prop-types as a dependency to avoid React 15.x warnings ([92598eb](https://bitbucket.org/atlassian/atlaskit/commits/92598eb))


null updates description for dynamic-table stories ([bde2081](https://bitbucket.org/atlassian/atlaskit/commits/bde2081))


* breaking; Table will now render the Header by default, even if no rows are displayed

## 3.0.0 (2017-05-24)


null refactor dynamic-table as part of styled-components refactor ([3f52aff](https://bitbucket.org/atlassian/atlaskit/commits/3f52aff))


* breaking; DynamicTable export renamed to DynamicTableStateless, for consistency with other packages.

ISSUES CLOSED: #AK-2385

## 2.1.5 (2017-04-27)


* fix; update legal copy to be more clear. Not all modules include ADG license. ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 2.1.4 (2017-04-26)


* fix; update legal copy and fix broken links for component README on npm. New contribution and ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))

## 2.1.3 (2017-03-23)


* fix; Empty commit to release the component ([49c08ee](https://bitbucket.org/atlassian/atlaskit/commits/49c08ee))

## 2.1.1 (2017-03-21)

## 2.1.1 (2017-03-21)


* fix; maintainers for all the packages were added ([261d00a](https://bitbucket.org/atlassian/atlaskit/commits/261d00a))

## 2.1.0 (2017-03-08)


* feature; use createError helper function from util-common package ([3466262](https://bitbucket.org/atlassian/atlaskit/commits/3466262))

## 2.0.1 (2017-03-08)


* fix; import DynamicTable from its own package ([61f6e86](https://bitbucket.org/atlassian/atlaskit/commits/61f6e86))

## 1.0.0 (2017-03-07)


* fix; pR suggestions ([aee611e](https://bitbucket.org/atlassian/atlaskit/commits/aee611e))


* feature; dynamicTable component ([9861b3e](https://bitbucket.org/atlassian/atlaskit/commits/9861b3e))
